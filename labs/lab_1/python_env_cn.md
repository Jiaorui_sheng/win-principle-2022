# Python 环境

- [如何在Visual Studio中创建和管理Python环境](
https://learn.microsoft.com/en-us/visualstudio/python/managing-python-environments-in-visual-studio)

*****************************

## 索引

1. [Python环境窗口](#the-python-environments-window) \
   [0.1 安装](#installation) \
   [0.2 dist-packages和site-packages区别](#dist_n_site)
2. [手动识别现有环境](#manually-identify-an-existing-environment) \
   [1.1 Creating Package创建包](#Creating-Package) \
   [1.2 Understanding `__init__.py`](#Understanding) \
   [1.3 Import Modules from a Package从包中导入模块](#Import-Modules-from-a-Package)
3. [恢复或删除无效环境](#fix-or-delete-invalid-environments) \
   [2.1 创建发布](#create-setup) \
   [2.2 安装发布](#install-setup)
4. [修改注册表以更正没有修复选项的环境，或是删除无效的环境](#modify-the-registry-to-correct-an-environment)

Python环境是运行Python代码的上下文，包括全局环境、虚拟环境和conda环境。环境由一个解释器、一个库（通常是Python标准库）和一组已安装的包组成。这些组件共同确定了有效的语法、可访问的操作系统功能以及可使用的包。

在Windows系统上的Visual Studio中，你可以使用本文中描述的“Python环境窗口“来管理环境，并选择一个新项目的默认环境。关于环境的其他内容可在以下文章中找到：

-  对于任何给定的项目，您可以[选择特定的环境](https://docs.microsoft.com/en-us/visualstudio/python/selecting-a-python-environment-for-a-project) 而不是使用默认环境。
- 有关为Python项目创建和使用虚拟环境的详细信息，请参见 [使用虚拟环境](https://docs.microsoft.com/en-us/visualstudio/python/selecting-a-python-environment-for-a-project?view=vs-2022#use-virtual-environments)。
- If you want to install packages in an environment, refer to the [Packages tab reference](https://docs.microsoft.com/en-us/visualstudio/python/python-environments-window-tab-reference?view=vs-2022#packages-tab).
- 安装其他Python解释器，请参见“下载Python解释器”选项卡参考[下载Python解释器](https://docs.microsoft.com/en-us/visualstudio/python/installing-python-interpreters?view=vs-2022)。通常，如果您下载并运行Python发行版的安装程序，Visual Studio将会检测到新安装和环境，新环境将出现在“Python环境”窗口中，你可以在项目中选择使用。

如果您不熟悉 Visual Studio 中的 Python，以下文章还提供了一些常规内容：
- [在 Visual Studio 中使用 Python](https://docs.microsoft.com/en-us/visualstudio/python/overview-of-python-tools-for-visual-studio?view=vs-2022)
- [在 Visual Studio 中安装 Python 支持](https://docs.microsoft.com/en-us/visualstudio/python/installing-python-support-in-visual-studio?view=vs-2022)

> **注** \
>
> 您可以通过“文件”>“打开”>“文件夹”来管理作为文件夹打开的 Python 代码的环境。 Python 工具栏允许您在所有检测到的环境之间切换，还可以添加新环境。 环境信息存储在工作区 .vs 文件夹中的 PythonSettings.json 文件中。

[返回索引](#index)


## 1 Python环境窗口
---------------

Visual Studio 检测到的环境显示在 Python 环境窗口中。打开窗口可以使用以下方法：

- 选择 `View > Other Windows > Python Environments` 菜单命令。
- 在“解决方案资源管理器”中右键单击项目的 `Python 环境` ，然后选择`查看全部Python环境`。

在所有这些情况下，Python 环境窗口都会出现在解决方案资源管理器旁边：\
![environments-default-view-2022.png](pix/environments-default-view-2022.png)

Visual Studio使用注册表（遵循[PEP 514](https://www.python.org/dev/peps/pep-0514/))）以及虚拟环境和conda环境（请参见[环境类型](https://docs.microsoft.com/en-us/visualstudio/python/managing-python-environments-in-visual-studio?view=vs-2022#types-of-environments)）查找已安装的全局环境。如果在列表中没有看到预期的环境，请参见[手动识别现有环境](https://docs.microsoft.com/en-us/visualstudio/python/managing-python-environments-in-visual-studio?view=vs-2022#manually-identify-an-existing-environment).。

在列表中选择环境时，Visual Studio将该环境的各种属性和命令显示在“Overview”选项卡上。

例如，您可以在上图中看到，解释器的位置是`D:\Program Files (x86)\Microsoft Visual Studio\Shared\Python39_64`。“Overview”选项卡底部的四个命令分别在解释器运行时打开一个命令提示符。有关更多信息，请参见[Python Environments窗口选项卡参考2022-概述](https://docs.microsoft.com/en-us/visualstudio/python/python-environments-window-tab-reference?view=vs-2022#overview-tab)。

使用环境列表下方的下拉列表切换到不同的选项卡，如Packages和IntelliSense。[Python环境窗口选项卡参考](https://docs.microsoft.com/en-us/visualstudio/python/python-environments-window-tab-reference?view=vs-2022)中也介绍了这些选项卡。

选择环境不会改变其与任何项目之间的联系。列表中以粗体显示的默认环境是Visual Studio用于新项目的默认环境。要对新项目使用不同的环境，请使用`将此作为新项目的默认环境` 命令。在项目上下文中，您始终可以选择特定的环境。有关详细信息，请参见[为项目选择环境](https://docs.microsoft.com/en-us/visualstudio/python/selecting-a-python-environment-for-a-project?view=vs-2022)。

在每个被列出的环境的右侧，都有一个控件用于打开该环境的交互式窗口。

> **注**\
> 尽管 Visual Studio 拥有 system-site-packages 选项，但它不提供从 Visual Studio 中更改它的方法。

[返回索引](#index)


## 2 手动识别现有环境
---------------



[返回索引](#index)


## 3 恢复或删除无效环境
---------------



[返回索引](#index)


## 4 修改注册表以更正没有修复选项的环境，或是删除无效的环境
